module.exports = {
  getNumOfMatchesPerYear: function(dataOfMatchesPlayed) {
    const numOfMatchesPlayedPerYear = dataOfMatchesPlayed.reduce(
      (numOfMatchesPlayedPerYear, match) => {
        if (numOfMatchesPlayedPerYear[match['season']] === undefined) {
          numOfMatchesPlayedPerYear[match['season']] = 1;
        } else {
          numOfMatchesPlayedPerYear[match['season']]++;
        }
        return numOfMatchesPlayedPerYear;
      },
      {}
    );
    return numOfMatchesPlayedPerYear;
  },

  getNumOfMatchesWonPerTeam: function(dataOfMatchesPlayed) {
    const teamNames = [...new Set(dataOfMatchesPlayed.map(item => item.team1))];
    const years = [...new Set(dataOfMatchesPlayed.map(item => item.season))];
    //creating a matches won template for data of all the years set to 0
    let yearsTemplate = years.reduce((yearsTemplate, currYear) => {
      yearsTemplate[currYear] = 0;
      return yearsTemplate;
    }, {});
    let matchesWonTemplate = teamNames.reduce(
      (matchesWonTemplate, currTeam) => {
        matchesWonTemplate[currTeam] = Object.assign({}, yearsTemplate);
        return matchesWonTemplate;
      },
      {}
    );
    let matchesWonPerTeam = dataOfMatchesPlayed.reduce(
      (matchesWonPerTeam, currentMatch) => {
        if (currentMatch.winner !== '') {
          matchesWonPerTeam[currentMatch.winner][currentMatch.season] += 1;
        }
        return matchesWonPerTeam;
      },
      matchesWonTemplate
    );
    return matchesWonPerTeam;
  },

  getExtraRunsConcededPerTeam: function(
    dataOfMatchesPlayed,
    dataOfBallsDelivered
  ) {
    const extraRuns = getDeliveriesForCertainYear(
      dataOfMatchesPlayed,
      dataOfBallsDelivered,
      '2016'
    ).reduce((extraRuns, delivery) => {
      if (extraRuns[delivery['bowling_team']] === undefined) {
        extraRuns[delivery['bowling_team']] = parseInt(delivery['extra_runs']);
      } else {
        extraRuns[delivery['bowling_team']] += parseInt(delivery['extra_runs']);
      }
      return extraRuns;
    }, {});
    return extraRuns;
  },

  getBestEconomicalBowlers: function(
    dataOfMatchesPlayed,
    dataOfBallsDelivered
  ) {
    mostEconomicBowler = getDeliveriesForCertainYear(
      dataOfMatchesPlayed,
      dataOfBallsDelivered,
      '2015'
    ).reduce((mostEconomicBowler, delivery) => {
      if (mostEconomicBowler[delivery['bowler']] === undefined) {
        mostEconomicBowler[delivery['bowler']] = {};
        mostEconomicBowler[delivery['bowler']]['balls'] = 1;
        mostEconomicBowler[delivery['bowler']]['runs'] =
          parseInt(delivery['wide_runs']) +
          parseInt(delivery['noball_runs']) +
          parseInt(delivery['batsman_runs']);
        mostEconomicBowler[delivery['bowler']]['economy'] =
          mostEconomicBowler[delivery['bowler']]['runs'] /
          (mostEconomicBowler[delivery['bowler']]['balls'] / 6);
      } else {
        mostEconomicBowler[delivery['bowler']]['balls']++;
        mostEconomicBowler[delivery['bowler']]['runs'] +=
          parseInt(delivery['wide_runs']) +
          parseInt(delivery['noball_runs']) +
          parseInt(delivery['batsman_runs']);
        mostEconomicBowler[delivery['bowler']]['economy'] =
          mostEconomicBowler[delivery['bowler']]['runs'] /
          (mostEconomicBowler[delivery['bowler']]['balls'] / 6);
      }
      return mostEconomicBowler;
    }, {});
    const getTopEconomicBowler = Object.entries(mostEconomicBowler)
      .sort((a, b) => a[1].economy - b[1].economy)
      .filter(values => values[1]['balls'] > 29)
      .slice(0, 10)
      .reduce((getTopEconomicBowler, bowler) => {
        getTopEconomicBowler[bowler[0]] = parseFloat(
          bowler[1].economy.toFixed(2)
        );
        return getTopEconomicBowler;
      }, {});
    return getTopEconomicBowler;
  },

  getTopPlayerOfTheMatch: function(dataOfMatchesPlayed) {
    const playerOftheMatchData = dataOfMatchesPlayed.reduce(
      (playerOftheMatch, currentMatch) => {
        if (playerOftheMatch[currentMatch['player_of_match']] === undefined) {
          playerOftheMatch[currentMatch['player_of_match']] = 1;
        } else {
          playerOftheMatch[currentMatch['player_of_match']]++;
        }
        return playerOftheMatch;
      },
      {}
    );

    const top5PlayerOfTheMatch = Object.entries(playerOftheMatchData)
      .sort((a, b) => b[1] - a[1])
      .slice(0, 5)
      .reduce((playerOfMatch, currentValue) => {
        playerOfMatch[currentValue[0]] = currentValue[1];
        return playerOfMatch;
      }, {});
    return top5PlayerOfTheMatch;
  }
};

function getDeliveriesForCertainYear(
  dataOfMatchesPlayed,
  dataOfBallsDelivered,
  year
) {
  const matchid = dataOfMatchesPlayed
    .filter(data => data['season'] === year)
    .map(dataOfMatchesPlayed => dataOfMatchesPlayed['id']);

  getDeliveryDataForId = dataOfBallsDelivered.reduce(
    (DeliveryDataForId, delivery) => {
      if (
        parseInt(delivery['match_id']) >= matchid[0] &&
        parseInt(delivery['match_id']) <= matchid[matchid.length - 1]
      )
        DeliveryDataForId.push(delivery);
      return DeliveryDataForId;
    },
    []
  );
  return getDeliveryDataForId;
}
