const fs = require('fs');
let dataMatch = JSON.parse(fs.readFileSync('../data/match.json', 'utf-8'));
let dataDelivery = JSON.parse(
  fs.readFileSync('../data/delivery.json', 'utf-8')
);
const result = require('./iplProject.js');

fs.writeFileSync(
  '../output/numOfMatchesPlayedPerYearOutput.json',
  JSON.stringify(result.getNumOfMatchesPerYear(dataMatch), null, 2)
);

fs.writeFileSync(
  '../output/numOfMatchesWonPerTeamPerYearOutput.json',
  JSON.stringify(result.getNumOfMatchesWonPerTeam(dataMatch), null, 2)
);

fs.writeFileSync(
  '../output/extraRunsConcededPerTeamIn2016Output.json',
  JSON.stringify(
    result.getExtraRunsConcededPerTeam(dataMatch, dataDelivery),
    null,
    2
  )
);

fs.writeFileSync(
  '../output/getTopEconomicalBowlersIn2015Output.json',
  JSON.stringify(
    result.getBestEconomicalBowlers(dataMatch, dataDelivery),
    null,
    2
  )
);
//console.log(result.getTopPlayerOfTheMatch(dataMatch));
