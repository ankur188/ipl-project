function charts1() {
  //1
  fetch('../output/numOfMatchesPlayedPerYearOutput.json')
    .then(response => response.json())
    .then(data =>
      Highcharts.chart('container1', {
        title: {
          text: `Matches Played Per Year In IPL from ${
            Object.keys(data)[0]
          } - ${Object.keys(data)[Object.keys(data).length - 1]}`
        },

        subtitle: {
          text: 'Source: kaggle.com'
        },

        yAxis: {
          title: {
            text: 'Number of Matches'
          }
        },
        xAxis: {
          title: {
            text: 'Years'
          },
          categories: Object.keys(data)
        },

        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
        },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: '{point.y}'
            }
          }
        },
        series: [
          {
            name: 'Matches Played',
            colorByPoint: true,
            data: Object.entries(data)
          }
        ],

        responsive: {
          rules: [
            {
              condition: {
                maxWidth: 500
              },
              chartOptions: {
                legend: {
                  layout: 'horizontal',
                  align: 'center',
                  verticalAlign: 'bottom'
                }
              }
            }
          ]
        }
      })
    )
    .catch(error => console.log('error ' + error));

  //2
  fetch('../output/extraRunsConcededPerTeamIn2016Output.json')
    .then(response => response.json())
    .then(data1 =>
      Highcharts.chart('container2', {
        chart: {
          type: 'column'
        },
        title: {
          text: 'Extra Runs Conceded Per Team In 2016'
        },
        subtitle: {
          text: 'Source: kaggle.com'
        },
        accessibility: {
          announceNewData: {
            enabled: true
          }
        },
        xAxis: {
          title: {
            text: 'Teams'
          },
          categories: Object.keys(data1)
        },
        yAxis: {
          title: {
            text: 'Runs Conceded'
          }
        },
        legend: {
          enabled: false
        },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: '{point.y}'
            }
          }
        },

        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat:
            '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        series: [
          {
            name: 'Browsers',
            colorByPoint: true,
            data: Object.values(data1)
          }
        ]
      })
    )
    .catch(error => console.log('error ' + error));
  //3

  fetch('../output/getTopEconomicalBowlersIn2015Output.json')
    .then(response => response.json())
    .then(data2 =>
      Highcharts.chart('container3', {
        chart: {
          type: 'bar'
        },
        title: {
          text: 'Top Economic Bowler In 2015'
        },
        subtitle: {
          text: 'Source: kaggle.com'
        },
        accessibility: {
          announceNewData: {
            enabled: true
          }
        },
        xAxis: {
          title: {
            text: 'Player'
          },
          categories: Object.keys(data2)
        },
        yAxis: {
          title: {
            text: 'Economy'
          }
        },
        legend: {
          enabled: false
        },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: '{point.y:.1f}%'
            }
          }
        },

        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat:
            '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        series: [
          {
            name: 'Browsers',
            colorByPoint: true,
            data: Object.values(data2)
          }
        ]
      })
    )
    .catch(error => console.log('error ' + error));

  //4

  fetch('../output/numOfMatchesWonPerTeamPerYearOutput.json')
    .then(response => response.json())
    .then(data3 =>
      Highcharts.chart('container4', {
        chart: {
          type: 'column'
        },
        title: {
          text: 'Matches Won Per Team Per Year'
        },
        subtitle: {
          text: 'Source: kaggle.com'
        },
        xAxis: {
          title: {
            text: 'Teams'
          },
          categories: [
            ...new Set(
              Object.values(data3)
                .map(values => Object.keys(values))
                .reduce((acc, curr) => acc.concat(curr), [])
                .sort()
            )
          ]
        },
        yAxis: {
          // min: -1,
          title: {
            text: 'Years'
          },
          stackLabels: {
            enabled: true,
            style: {
              fontWeight: 'bold',
              color:
                // theme
                (Highcharts.defaultOptions.title.style &&
                  Highcharts.defaultOptions.title.style.color) ||
                'gray'
            }
          }
        },
        legend: {
          align: 'right',
          x: -30,
          verticalAlign: 'top',
          y: 25,
          floating: true,
          backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || 'white',
          borderColor: '#CCC',
          borderWidth: 1,
          shadow: false
        },
        tooltip: {
          headerFormat: '<b>{point.x}</b><br/>',
          pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
          column: {
            stacking: 'normal',
            dataLabels: {
              enabled: false
            }
          }
        },

        series: app(data3).reduce((team, winningData) => {
          team.push({
            name: winningData[0],
            data: winningData[1]
          });
          return team;
        }, [])
      })
    )
    .catch(error => console.log('error ' + error));
}
function app(matchesWonPerTeam) {
  const series = Object.entries(matchesWonPerTeam).reduce((acc, curr) => {
    const temp = [];
    temp.push(curr[0]);
    temp.push(Object.values(curr[1]));
    acc.push(temp);
    return acc;
  }, []);
  return series;
}
